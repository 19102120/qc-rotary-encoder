 #define EN        8

//Direction pin
#define X_DIR     5
#define Y_DIR     6
#define Z_DIR     7

//Step pin
#define X_STP     2
#define Y_STP     3
#define Z_STP     4

uint8_t doSteps = 0;

String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

//DRV8825
int delayTime = 1000; //Delay between each pause (uS)
int stps = 200; // Steps to move 1 full rotation

String getValue(String data, char separator, int index) {
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void parseCommand(String command) {
  Serial.println(command);
  Serial.println(command.indexOf("motor"));
  if (command.indexOf("motor") >= 0) {
    doSteps = (getValue(command, ',', 1)).toInt();
    return;
  }
}

void step(boolean dir, byte dirPin, byte stepperPin, int steps)

{

  digitalWrite(dirPin, dir);

  for (int i = 0; i < steps; i++) {

    digitalWrite(stepperPin, HIGH);

    delayMicroseconds(delayTime);

    digitalWrite(stepperPin, LOW);

    delayMicroseconds(delayTime);

  }

}

void setup() {

  pinMode(X_DIR, OUTPUT); pinMode(X_STP, OUTPUT);

  pinMode(Y_DIR, OUTPUT); pinMode(Y_STP, OUTPUT);

  pinMode(Z_DIR, OUTPUT); pinMode(Z_STP, OUTPUT);

  pinMode(EN, OUTPUT);

  digitalWrite(EN, LOW);
  
  Serial.begin(38400);
  inputString.reserve(100);

}

void loop() {

  if(doSteps){
    for(int x = 0; x < stps; x++) {
      digitalWrite(X_STP,HIGH);
      delayMicroseconds(750);
      digitalWrite(X_STP,LOW);
      delayMicroseconds(750);
      digitalWrite(Y_STP,HIGH);
      delayMicroseconds(750);
      digitalWrite(Y_STP,LOW);
      delayMicroseconds(750);
      digitalWrite(Z_STP,HIGH);
      delayMicroseconds(750);
      digitalWrite(Z_STP,LOW);
      delayMicroseconds(750);
    }
  }

  if (stringComplete) {
    inputString.trim(); //removes whitespace from both sides of a inputString
    parseCommand(inputString); //cal parseCommand with variable from inputString
    inputString = ""; //set empty
    stringComplete = false; //set stringComplete is false 
  }

//  delay(25);
}

void serialEvent() {
  while (Serial.available()) { 
    //  run when Serial is available
    
    char inChar = (char)Serial.read(); //inChar picking data from Serial
    Serial.println((int)inChar); //Print inChar value and convert to int datatype

    if (inChar != '\n' || inChar != '\r') { 
      inputString += inChar; //inputString = inputString + inChar
    }

    if (inChar == '\n' || inChar == '\r') {
      stringComplete = true; //set StringComplete is true
    }
  }
}
