 #include<Encoder.h>

Encoder rte(3,2); //using analog pin 3 and 2
long posAwal = 0;
const long ADDITIONALVALUE = 1000; //to set position 1000 tick on begining
void setup() {
  Serial.begin(38400); // set baudrate 38400 same with node js
//  Serial.println(posAwal);
}

void loop() {
  long posSekarang = rte.read() + ADDITIONALVALUE;
  if(posSekarang != posAwal){
    posAwal = posSekarang; 
    Serial.println("{\"counter\":"+ String(posAwal) +"}"); //format message to NodeJS
}
}
