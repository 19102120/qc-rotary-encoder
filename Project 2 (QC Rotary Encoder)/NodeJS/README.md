# QC SOFTWARE ROTARY KNOB

*desc
- Program untuk menguji kelayakan Rotary Encoder Knob pada Ventilator dengan menghitung akurasi ticks sebagai parameternya.

*how to use
- setup rotary encoder yang akan diuji
- setting port sesuai dengan nomor port PC (cek device manager) pada file app.config.js 
- run rm .\node_modules\ untuk menghapus module node JS
- run npm i untuk initialize dan melakukan instalasi.
- run npm start untuk menjalankan program.
- ketika aplikasi electron berjalan, klik start untuk memulai pengujian rotary.

*fitures
- menampilkan durasi pengujian rotary encoder knob
- menampilkan total ticks pengujian rotary encoder knob
- menampilkan consistency pengujian rotary encoder knob
- menampilkan average duration pengujian rotary encoder knob