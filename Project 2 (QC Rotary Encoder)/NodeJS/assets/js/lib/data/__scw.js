class __scw {
	init(_list) {
		this._SCWs = _list;
		this._steps = [
			{
				auth: [
					{
						name: ['pengawas'],
						group: 0,
						alias: undefined
					}
				]
			},
			{
				auth: [
					{
						name: ['utility'],
						group: 0,
						alias: undefined
					}
				]
			},
			{
				auth: [
					{
						name: ['utility'],
						group: 0,
						alias: undefined
					}
				]
			},
			{
				auth: [
					{
						name: [['pengawas', 'operator']],
						group: 1,
						alias: 'PRODUCTION'
					}
				]
			},
			{
				auth: [
					{
						name: ['utility'],
						group: 0,
						alias: undefined
					},
					{
						name: [['pengawas', 'operator']],
						group: 1,
						alias: 'PRODUCTION'
					}
				]
			}
		];
	};

	details(id) {
		const listSCW = this.list;
		for (const parent of listSCW) {
			for (const item of parent.items) {
				if (item.id === id) {
					return {
						...item,
						category: parent.category
					};
				}
			}
		}
	};

	start(id, lastStepDuration) {
		this._problem = {
			...this.details(id),
			stepDurations: [lastStepDuration],
			step: 1
		};

		return this._problem;
	};

	async finish(duration) {
		const data = {
			...this._problem,
			duration: this.totalDuration
		};

		delete this._problem;

		return data;
	};

	nextStep(lastStepDuration) {
		// if Problem is empty throw erro
		if (!this._problem) {
			throw 'SCW Problem is empty';
		}

		const len = this._steps.length - 1;

		this._problem.stepDurations.push(lastStepDuration);
		this._problem.onLastStep = this._problem.step >= len - 1;

		if (this._problem.step < len) {
			this._problem.step++;
			return true;
		}

		this._problem.step = len + 1;
		return false;
	};

	current(key) {
		const _current = this._problem ? this._problem : {};

		return key && _current ? _current[key] : _current;
	};

	reset() {
		this._problem = undefined;
	};

	get list() {
		return this._SCWs;
	};

	get problem() {
		return this._problem;
	}

	get totalDuration() {
		return this._problem && this._problem.stepDurations
			? this._problem.stepDurations.reduce((acc, val, idx) => acc + val, 0)
			: 0;
	};

	get authorization() {
		const stepLen = this._steps.length;
		const currentStep = this._problem ? this._problem.step : 0;

		const positions = currentStep < stepLen
			? this._steps[currentStep].auth
			: this._steps[stepLen - 1].auth;

		return positions;
	};
}

module.exports = new __scw();
