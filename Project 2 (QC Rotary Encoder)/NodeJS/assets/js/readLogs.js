'use strict';
const __basedir = process.cwd();
const fs = require('fs');
const fsPromise = fs.promises;

const helper = require(`${__basedir}/class/helper.class.js`);

const dirLog = `${__basedir}/config/logs`;

function getAllLogs(directory){
	const logs = helper.getFilesInFolderSync(directory, '.json');

	const contents = []
	for(const log of logs){
		try{
			contents.push(...require(log));
		} catch(error) {
			console.error(`Error at "${log}"`, error);
		}
	}

	return contents;
}

(async () => {
	const logs = getAllLogs(dirLog);

	const summary = {
		ticks: 0,
		duration: 0,
		ticksPerSecond: 0,
	};

	let csvString = 'total ticks,total duration,Avg ticks/second,ticks per round,duration per round,ticks/second\n';
	let ctr = 1;
	for(const item of logs){
		summary.ticks += item.ticks;
		summary.duration += item.duration;
		summary.ticksPerSecond += item.ticksPerSecond;

		csvString += `${summary.ticks},${helper.msToHms(summary.duration, 1)},${summary.ticksPerSecond/ctr},${item.ticks},${helper.msToHms(item.duration, 1)}, ${item.ticksPerSecond}\n`;
		ctr++;
	}

	const additional = {
		ticks: 35,
		duration: (15 * 60000),
		get ticksPerSecond(){
			return this.ticks / this.duration;
		},
	};

	summary.ticks += additional.ticks;
	summary.duration += additional.duration;
	summary.ticksPerSecond += (additional.ticks / additional.duration);

	csvString += `${summary.ticks},${helper.msToHms(summary.duration, 1)},${summary.ticksPerSecond/ctr},${additional.ticks},${helper.msToHms(additional.duration, 1)}, ${additional.ticksPerSecond}\n`;
	ctr++;

	summary.ticksPerSecond = summary.ticksPerSecond / logs.length;
	summary.durationHms = helper.msToHms(summary.duration, 1);

	console.table(summary);
	fs.writeFileSync(`${dirLog}/log-${helper.formatDate(new Date(), 'YYYY-MM-DD_HHmmss')}.csv`, csvString);
})();
