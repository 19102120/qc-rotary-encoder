'use strict';
const __basedir = process.cwd();

const config = require(`${__basedir}/config/app.config.js`);
const _serial = require('@iqrok/serial.helper');
const fs = require('fs');
const { ipcRenderer } = require('electron');

const helper = require(`${__basedir}/class/helper.class.js`);
const ui = require(`${__basedir}/assets/js/lib/ui.lib.js`);
const display = require(`${__basedir}/assets/js/lib/display.lib.js`);
const timer = require(`${__basedir}/assets/js/lib/timer.lib.js`);

// PERHITUNGAN NILAI KONTEN
function ROUND_SUMMARY(ticks, duration, last){
	const lastDuration = last.duration == undefined ? 0 : last.duration;

	this.timestamp = Date.now();
	this.ticks = ticks;
	this.duration = duration;
	this.diff = Math.abs(duration - lastDuration);
	this.ticksPerSecond = ticks / (duration / 1000);
}

// NILAI AWAL KONTEN
function SAMPLING_TEMPLATE(){
	return {
		timer: {
			start: null,
			betweenTicks: null
		},

		counter: {
			last: null,
			sum: 0
		},

		variables:{
			totalTicks: {
				title: 'Total Ticks',
				unit: 'ticks',
				value: 0,
			},

			duration: {
				title: 'Duration',
				unit: '',
				value: 0,
				formatter: (val) => {
					return helper.msToHms(val, true);
				},
			},

			jitter: {
				title: 'Consistency',
				unit: 'ms',
				value: 0,
			},

			avgDuration: {
				title: 'Average Duration',
				unit: '',
				value: 0,
				formatter: (val) => {
					return helper.msToHms(val, true);
				},
			},
		},

		_isStarted: false,
		_delayOnStart: 2000,
		_startTimestamp: Date.now(),
		_base: 0,
	};
};

let forceStop = false;
let sampling = [[], [], [], []];
for(const key in config.arduino){
	sampling[key] = new SAMPLING_TEMPLATE();
}

const samplingData = [[], [], [], []];

const arduinos = []
for(const key in config.arduino){
	arduinos.push(new _serial(config.arduino[key], true));
}
const motor = new _serial(config.motor, true);

const dirLog = `${__basedir}/config/logs`;
const { limits, target } = config;
const lastTickFile = `${dirLog}/.lastTicks`;

const roundTwo = (num) => {
	return Math.round((num + Number.EPSILON) * 100) / 100
}

// MENGAMBIL TICK TERAKHIR
function getLastTick(){
	if(!fs.existsSync(`${lastTickFile}`)){
		return null;
	}

	const lastTick = fs.readFileSync(`${lastTickFile}`);

	return +lastTick.toString();
}

// MENGATUR TICK TERAKHIR
function setLastTick(value){
	return fs.writeFileSync(`${lastTickFile}`, value);
}

// MENGHAPUS TICK TERAKHIR
function removeLastTick(){
	try{
		return fs.unlinkSync(`${lastTickFile}`);
	} catch(error){
		console.log('removeLastTick Caught error', error);
	}
}

// MEMPERBARUI NILAI TITLE DAN UNIT 
function updateTitleAndUnit(){
	for(const arduino in sampling){
		for(const key in sampling[arduino].variables) {
			const item = sampling[arduino].variables[key];
	
			$(`.variables-${arduino}-${key} .title`).html(item?.title);
			$(`.variables-${arduino}-${key} .unit`).html(item?.unit);
			$(`.variables-${arduino}-${key} .value`).html(item?.value);
		}
	}
}

// UPDATE NILAI DARI ARDUINO 
function updateValue(){
	for(const arduino in sampling){
		for(const key in sampling[arduino].variables) {
			const item = sampling[arduino].variables[key];
			const val = item?.formatter === undefined
				? item?.value
				: item.formatter(item?.value);

			$(`.variables-${arduino}-${key} .value`).html(val);
		}
	}
}

// MENGHITUNG KONSISTENSI
function calculateConsistency(list){
	const len = list.length;

	let jitter = 0;
	let sum = 0;
	let counter = 0;
	for(let idx = 1; idx < len; idx++){
		const item = list[idx];
		sum += item.duration;
		jitter += item.diff;
		counter++;
	}

	if(sum == 0 && counter == 0 && jitter == 0){
		return {
			duration: 0,
			jitter: 0,
		};
	}

	return {
		duration: sum / counter,
		jitter: jitter / counter,
	};
}

// MENAMBAH BOX HASIL PERHITUNGAN
function addResultBox(collection, avg, index){
	const box = $(`.results-box-${index}`);

	const limitJitter = helper.round(avg.duration * 2.5 / 100);

	if(isNaN(limitJitter)){
		return;
	}

	const errorTolerance = target.ticksPerSecond * target.tolerance;
	const errorTicksPerSecond = Math.abs(
			collection.ticksPerSecond - target.ticksPerSecond
		);

	const isGood = errorTicksPerSecond	<= errorTolerance;

	const bgColor = isGood ? `bg-success text-white` : `bg-success text-white`;

	const html = `<div class="col mt-2 ${bgColor} border rounded text-center">
			<h6>${collection.ticks} ticks</h6>
			<h6>${helper.msToHms(collection.duration, true)}</h6>
			<h6>${collection.diff} ms</h6>
			<h6>${helper.round(collection.ticksPerSecond, 3)} ticks/s</h6>
			<h6>Error: ${helper.round(errorTicksPerSecond, 3)} ticks/s</h6>
		</div>`;

	box.append(html);
}

// MULAI UJI
function startRound(){
	forceStop = false;
	motor.println('motor,1');
	$('.start-motor').hide();
	$('.stop-motor').removeClass('hidden');
	forceStop = false;
}

// BERHENTI UJI 
function stopRound(immediate = false, samplingIndex){
	const delay = immediate ?  250 : 1200000;
	//motor.println('motor,0');
	sampling[samplingIndex]._isStarted = false;

	$('.stop-motor').addClass('hidden');

	const logpath = `${dirLog}/`
			+`${helper.formatDate(new Date, 'YYYY-MM-DD_HHmmss')}.json`;

	fs.writeFileSync(logpath, JSON.stringify(samplingData[samplingIndex], null, '\t'));

	setTimeout(() => {
		ipcRenderer.send('app-reload');
	}, delay);
}

/********************** DOM OPERATIONS *************************/
ui.DOMContentLoaded(async function () {
	// clear localStorage just in case something is already saved
	localStorage.clear();

	// initialize template
	await ui.init();

    ui.documentReady();

    updateTitleAndUnit();

    ui.showSection('main');
});

ui.ready(() => {
	const timeStarted = Date.now();
	const lastTick = getLastTick();

	for(const [index, arduino] of Object.entries(arduinos)){
		arduino.on('data', received => {
			const { counter } = received.data;
			
			if (counter == undefined || Date.now() - timeStarted < sampling[index]._delayOnStart) {
				return;
			}
			// console.log('arduino [', index, '] : ', counter);

			if(typeof(counter) == 'number' && !sampling[index]._isStarted){
				if(counter != sampling[index].counter.last){
					sampling[index]._isStarted = true;
					sampling[index]._startTimestamp = Date.now();
				}

				sampling[index].counter.last = counter;
			}

			const timestamp = Date.now();
			if (sampling[index].timer.start !== null) {
				sampling[index].timer.betweenTicks = timestamp - sampling[index].timer.start;
			}

			sampling[index].timer.start = timestamp;
			sampling[index].counter.sum += (counter - sampling[index].counter.last);
			sampling[index].counter.last = counter;

			sampling[index].variables.totalTicks.value = sampling[index].counter.sum;
			sampling[index].variables.duration.value = sampling[index]._isStarted
										? Date.now() - sampling[index]._startTimestamp
										: 0;

			if(sampling[index].counter.sum >= limits.round || forceStop){
				const lastData = samplingData[index][samplingData[index].length - 1] || {
							duration: undefined,
						};

				const collection = new ROUND_SUMMARY(
						sampling[index].variables.totalTicks.value,
						sampling[index].variables.duration.value,
						lastData,
					);

				samplingData[index].push(collection);

				sampling[index] = new SAMPLING_TEMPLATE();

				const avg = calculateConsistency(samplingData[index]);

				sampling[index].variables.jitter.value = roundTwo(avg.jitter);
				sampling[index].variables.avgDuration.value = avg.duration;

				addResultBox(collection, avg, index);
				setLastTick(+lastTick + sampling[index].counter.sum);

				console.log(samplingData[index]);

				if(limits.counter++ >= limits.total || forceStop){
					stopRound(false, index);
				}
			}
		});

		arduino.on('open', received => {
			console.log(received);
		});

		arduino.on('error', received => {
			console.log(received);
		});
	}

	motor.on('data', received => {
		//~ console.log('motor', received);
	});

	motor.on('error', received => {
		console.log(received);
	});

	// MENGATUR HASIL INTERVAL PENGUJIAN 
	setInterval(() => {
		let currentDuration = [0, 0, 0, 0];
		for(const index in sampling){
			currentDuration[index] = sampling[index]._isStarted
				? Date.now() - sampling[index]._startTimestamp
				: 0;
		}
		for(const index in sampling){
			sampling[index].variables.duration.value = currentDuration[index];
		}
		updateValue();

		for(const index in sampling){
			if(currentDuration[index] > limits.timeout){
				stopRound(false, index);
				$('.log-total-ticks').html(+lastTick + sampling[index].counter.sum);
				ui.showPopup('abnormality');
			}
		}

	}, 10);

	// NOTIFIKASI KETIKA MEMBUKA PROGRAM  
	ui.showNotification('start after 15 secs', 'success', 5000);

	// setTimeout(() => {
	// 	startRound();
	// }, 15000);
});

ui.ready(() => {
	$('.start-motor').click(function(){
		startRound();
	});

	$('.stop-motor').click(function(){
		removeLastTick();
		forceStop = true;
	});
});
