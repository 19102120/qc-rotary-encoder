global.__basedir = process.cwd();

const { app, BrowserWindow, ipcMain, remote } = require('electron');
const { spawn } = require('child_process');
const fs = require('fs');
const path = require('path');

// app config
const config = require(`${__basedir}/config/app.config`);

if (config.autoreload) {
	// Enable live reload for all the files inside your project directory
	require('electron-reload')(__dirname, {
		ignored: [/config\/logs\/|node_modules|[/\\]\./]
	});
}

let mainWindow;

// to mitigate compatibilty electron 9 with serialport
app.allowRendererProcessReuse = false;

function createWindow() {
	// Create the browser window.
	mainWindow = new BrowserWindow({
		width: config.window.width,
		height: config.window.height,
		frame: false,
		fullscreen: config.window.fullscreen,
		icon: './assets/img/favicon.png',
		webPreferences: {
			nodeIntegration: true,
			contextIsolation: false,
			enableRemoteModule: true,

			// use ternary because cant accept undefined as false
			devTools: config.devTools
				? config.devTools
				: true,
		}
	});

	// and load the index.html of the app.
	mainWindow.loadFile('index.html');

	// Emitted when the window is closed.
	mainWindow.on('closed', function() {
		mainWindow = null;
	});
}
// app.allowRendererProcessReuse = false
app.on('ready', createWindow);

//~ app.whenReady(() => {
	//~ app.allowRendererProcessReuse = false;
//~ });

// Quit when all windows are closed.
app.on('window-all-closed', function() {
	if (process.platform !== 'darwin') app.quit();
});

app.on('activate', function() {
	if (mainWindow === null) createWindow();
});

ipcMain.on('app-reload', (event, args) => {
	for (const window of BrowserWindow.getAllWindows()) {
		if (window.webContents) {
			window.webContents.reloadIgnoringCache();
		}
	}
});
