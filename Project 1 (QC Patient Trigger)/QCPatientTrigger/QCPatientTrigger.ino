/**
 * by Savior Team under PT.Stechoq Robotika Indonesia
 * 
 * This program used for Arduino Uno that controlling  
 * Quality Controlling Patient Trigger Ventilator
 */

#include <stdio.h>  
//getting the input from the user(Keyboard) and output result text to the monitor(screen)
#include <stdlib.h> 
//getting standard library

const int toogleswitch = 2;
//toogle using digital pin 2        
const int vacuum =7;        
//vacuum using digital pin 7        
const int solenoid = 6;     
// solenoid using digital pin 6
int switch1=0;              
// switch using digital pin 0

void setup() {
  Serial.begin(9600); 
  //set Baudrate 9600
  pinMode(toogleswitch,INPUT);  
  //set togleswitch pin as INPUT   
  pinMode(vacuum,OUTPUT);     
  //set Vacuum pin digital 7 as OUTPUT
  pinMode(solenoid, OUTPUT); 
  //set selenoid valve pin digital 6 as OUTPUT
}

void loop() {
  switch1=digitalRead(toogleswitch); 
  //switch1 picking value from toogleswitch(2) pin
  
    if (switch1==0){ 
      //when switch is "0" or OFF

      digitalWrite(vacuum,LOW); 
      digitalWrite(solenoid, LOW);
      //then set Vacuum and Selenoid to LOW of OFF
      }

                               
    else if (switch1==1 ) {
      //but when switch is "1" or ON
         
      digitalWrite(vacuum,LOW);
      //set vacuum to LOW/OFF
      
      digitalWrite(solenoid, LOW );
      //set vacuum to LOW/OFF
      delay(500);
      //delay 500ms of 0,5 second
      
      digitalWrite(solenoid,HIGH);
      //set vacuum to HIGH/ON
      
      digitalWrite(vacuum,HIGH);
      //set vacuum to HIGH/ON
      
      delay(4000);
      //delay 4000ms or 4 sec
      }
  }                            
