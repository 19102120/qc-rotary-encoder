/**
 * The microcontroller on the Arduino Nano have 512 bytes of EEPROM: 
 * memory whose values are kept when the board is turned off (like a tiny hard drive).
 * 
 * Integer data needs two byte spaces 
 * 
 * Integer data parsed two part and saved by byte data type 
 * 
 * EEPROM address data/buffer is start from 0
 * Arduino Nano have 512 bytes of EEPROM: 
 * 
 * To read int from EEPROM, you just need two part of data address
 */

 
byte EEPROM_READ_INT(int address) {

  int two = EEPROM.read(address);
  
  int one = EEPROM.read(address);
  // 0xFF = 
  // 0xFFFF = 
  return ((two << 0) & 0xFF) + ((one << 8) & 0xFFFF);
  //shift two to 0, one to 8 (binary)
}
