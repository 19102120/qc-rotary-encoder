/**
 * The microcontroller on the Arduino Nano have 512 bytes of EEPROM: 
 * memory whose values are kept when the board is turned off (like a tiny hard drive).
 * 
 * Integer data needs two byte spaces 
 * LongInt needs 4 byte spaces
 * 
 * Integer data parsed two part and saved by byte data type 
 * 
 * EEPROM data address start from 0
 */

void EEPROM_WRITE_INT(int address, int value) {
  
  byte two = (value & 0xFF); 
  
  byte one = ((value >> 8) & 0xFF);
  
  EEPROM.write(address, two);

  //+1 to save to next address buffer
  EEPROM.write(address + 1, one);

}
