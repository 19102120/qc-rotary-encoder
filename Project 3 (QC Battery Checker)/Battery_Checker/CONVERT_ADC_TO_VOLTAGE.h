/**
 * Calculate voltage from ADC value
 * 
 */

void CONVERT_ADC_TO_VOLTAGE(int index) {
  
  //read from analog pin 0, saved to variable adc_value1
  adc_value1 = analogRead(ANALOG_IN_PIN0); 
  
  // Determine voltage at ADC input
  adc_voltage1  = (adc_value1 * ref_voltage) / maxADC; 

  // Calculate voltage at divider inputyr
  in_voltage1 = (adc_voltage1 / (R2 / (R1 + R2)))-1 ;

  //print voltage value to mySerial(LCD)
  mySerial.print(in_voltage1);

  // add voltage unit
  mySerial.print(" V"); 
}
