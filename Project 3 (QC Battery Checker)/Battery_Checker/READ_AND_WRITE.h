  /**
   * EEPROM (Electronic Erasable Programmable Read Only Memory)
   * Used for saving int data to EEPROM of Arduino
   * Arduino EEPROM memory (Nano = 512 byte, Uno = 1kB, Mega = 4 kB) 
   * 
   * firstPart = EEPROM firstPart byte for saving data
   * secondPart = EEPROM secondPart byte for saving data
   * index = index of data, there 6 data in array (0-5) that will be saved in EEPROM IC
  **/

// Analog pin 0 as ANALOG_IN_PIN0
#define ANALOG_IN_PIN0 A0

//initialize with byte data type, because will saved to EEPROM 
byte tmpNilai = 0;
byte tmpNilai2 = 0;

int adc_value1 = 0;
float adc_voltage1 = 0.0; 
float in_voltage1 = 0.0;

// Floats for resistor values in voltage divider (Ohms unit)
float R1 = 10000.00;
float R2 = 2000.00;

// Float for Reference Voltage
float ref_voltage = 5.00 ;

//max ADV value from analog pins
int maxADC = 1023;


void READ_AND_WRITE(int firstPart, int secondPart, int index) {

  //read from analog pin 0
  adc_value1 = analogRead(ANALOG_IN_PIN0); 
  
  adc_voltage1  = (adc_value1 * ref_voltage) / maxADC;

  // Calculate voltage at divider inputyr
  in_voltage1 = (adc_voltage1 / (R2 / (R1 + R2)))-1 ;

  //Writing int data first part(byte).
  //To pick 1st and 2nd digit of 4th digit integer
  EEPROM_WRITE_INT((firstPart), adc_value1 / 100);

  
  //Writing int data second part(byte).
  //To pick 3st and 4nd digit of 4th digit integer
  EEPROM_WRITE_INT((secondPart), adc_value1 % 100);

  //first part (byte) saved to tmpNilai
  tmpNilai = EEPROM_READ_INT(firstPart);

  //first part (byte) saved to tmpNilai2
  tmpNilai2 = EEPROM_READ_INT(secondPart);

  // merge parsed data from two address EEPROM
  // * 100 to set fisrt part to be 1st and 2nd digit of 4th digit integer
  int newResult = (int)tmpNilai * 100 + (int)tmpNilai2; 

  //print full Result
  mySerial.print(newResult); 
}
