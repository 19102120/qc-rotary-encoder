/**
 * 
 */
 
#define EE_NILAI0 0
#define EE_NILAI1 1

#define EE_NILAI2 2
#define EE_NILAI3 3

#define EE_NILAI4 4
#define EE_NILAI5 5

#define EE_NILAI6 6
#define EE_NILAI7 7

#define EE_NILAI8 8
#define EE_NILAI9 9

#define EE_NILAI10 10
#define EE_NILAI11 11

const int rellay1 = 2;
const int rellay2 = 3;
const int rellay3 = 4;
const int rellay4 = 5;
const int rellay5 = 6;
const int rellay6 = 7;


int delayON = 4000;
int delayread = 1000;

  
  //function to check and read ADC value
void CHECK_ALL() {
  
  //Show last saved data
  //format to set text in Nextion LCD = (objname).txt="(value/variable)"
  mySerial.print("t12.txt=\""); //t12 is objname of ADC min LCD
  READ_INT_DATA(EE_NILAI0, EE_NILAI1, 0); //call function
  mySerial.print("\""); //to add symbol (")
  mySerial.write(0xff);
  mySerial.write(0xff); //writing to mySerial
  mySerial.write(0xff);
  mySerial.print("t13.txt=\""); //t13 is objname of ADC max LCD
  READ_INT_DATA(EE_NILAI10, EE_NILAI11, 5); //call function
  mySerial.print("\""); //to add symbol (")
  mySerial.write(0xff); 
  mySerial.write(0xff); //writing to mySerial
  mySerial.write(0xff);


  //UNIT 1 (New Data)
  digitalWrite(rellay1, LOW); //set relay ON
  delay(delayON); //delay before reading ADC value
  mySerial.print("t7.txt=\"");
  READ_AND_WRITE(EE_NILAI0, EE_NILAI1, 0); //call function READ_AND_WRITE(EEPROM byte-0,EEPROM byte-1) index 0.
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);

  mySerial.print("t0.txt=\"");
  CONVERT_ADC_TO_VOLTAGE(0); // call voltage calculation function
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);

  delay(delayread); //delay after read data
  digitalWrite(rellay1, HIGH); //set relay 1 OFF again

  // UNIT 2 (New Data)
  digitalWrite(rellay2, LOW); //set relay 2 ON 
  delay(delayON); //delay before 
  mySerial.print("t8.txt=\"");
  READ_AND_WRITE(EE_NILAI2, EE_NILAI3, 1); //call function READ_AND_WRITE(EEPROM byte-2,EEPROM byte-3) index 1
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.print("t1.txt=\"");
  CONVERT_ADC_TO_VOLTAGE(1); // call voltage calculation function
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(delayread); //delay after read data
  digitalWrite(rellay2, HIGH); //set relay 1 OFF again

  //  UNIT 3 (New Data)
  digitalWrite(rellay3, LOW); //set relay 2 ON 
  delay(delayON); 
  mySerial.print("t11.txt=\"");
  READ_AND_WRITE(EE_NILAI4, EE_NILAI5, 2);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.print("t2.txt=\"");
  CONVERT_ADC_TO_VOLTAGE(2);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(delayread);
  digitalWrite(rellay3, HIGH);

  //  UNIT 4 (New Data)
  digitalWrite(rellay4, LOW);
  delay(delayON);
  mySerial.print("t6.txt=\"");
  READ_AND_WRITE(EE_NILAI6, EE_NILAI7, 3);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.print("t3.txt=\"");
  CONVERT_ADC_TO_VOLTAGE(3);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(delayread);
  digitalWrite(rellay4, HIGH);

  //  UNIT 5 (New Data)
  digitalWrite(rellay5, LOW);
  delay(delayON);
  mySerial.print("t9.txt=\"");
  READ_AND_WRITE(EE_NILAI8, EE_NILAI9, 4);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.print("t4.txt=\"");
  CONVERT_ADC_TO_VOLTAGE(4);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(delayread);
  digitalWrite(rellay5, HIGH);

  //UNIT 6 (New Data)
  digitalWrite(rellay6, LOW);
  delay(delayON);
  mySerial.print("t10.txt=\"");
  READ_AND_WRITE(EE_NILAI10, EE_NILAI11, 5);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.print("t5.txt=\"");
  CONVERT_ADC_TO_VOLTAGE(5);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(delayread);
  digitalWrite(rellay6, HIGH);
  delay(delayON);

  //Show New Saved Data
  mySerial.print("t14.txt=\"");
  READ_INT_DATA(EE_NILAI0, EE_NILAI1, 0);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.print("t15.txt=\"");
  READ_INT_DATA(EE_NILAI10, EE_NILAI11, 5);
  mySerial.print("\"");
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(delayON);
}
