/*
 * Reading int data from EEPROM
 * index 0-5 (6 data int)
 * 
 * 
 */

// Custom RX TX pin using Digital Pin.
// RX = Dgitial pin 9
// TX = Digital pin 10  
SoftwareSerial mySerial(9, 10); 


void READ_INT_DATA(int firstPart, int secondPart, int index) {
  
  //Reading data (byte) from first part
  byte tmpNilai = EEPROM_READ_INT(firstPart);
  
  //Reading data (byte) from second part
  byte tmpNilai2 = EEPROM_READ_INT(secondPart);
  
  //merge 2 data byte to 1 data int
  int newResult = (int)tmpNilai * 100 + (int)tmpNilai2; 
  
  //print merged data from EEPROM to Serial.
  mySerial.print(newResult); 
  
}
