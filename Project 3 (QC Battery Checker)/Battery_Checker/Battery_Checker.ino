/**
 * by Savior Team under PT.Stechoq Robotika Indonesia
 * 
 * This program used for Arduino Nano that controlling  
 * Quality Control Battery Calibrator
 * 
 * Arduino Nano reading Analog to Digital Converter value from 6 input (23,5 V - 28,5 V).
 * Value ADC and Voltage calculation showed up in Nextion LCD. 
 * 
 * Max and mini ADC value saved in EEPROM chip.
 * Previous saved data and new Saved data will showed up in LCD, 
 */


//using library
#include <EEPROM.h>
#include <SoftwareSerial.h>

//using code fromm another file
#include "EEPROM_READ_INT.h"
#include "EEPROM_WRITE_INT.h"
#include "READ_INT_DATA.h"
#include "READ_AND_WRITE.h"
#include "CONVERT_ADC_TO_VOLTAGE.h"
#include "CHECK_ALL.h"

int address = 0;


String response = "";

void setup() {
  Serial.begin(9600);     //Serial communication to Serial Monitor
  mySerial.begin(115200); //Serial comunication to LCD Nextion

  //set pin to OUTPUT mode
  pinMode(rellay1, OUTPUT);
  pinMode(rellay2, OUTPUT);
  pinMode(rellay3, OUTPUT);
  pinMode(rellay4, OUTPUT);
  pinMode(rellay5, OUTPUT);
  pinMode(rellay6, OUTPUT);

  //Set pin HIGH, to cut off Relay (Relay Normally Close) 
  digitalWrite(rellay1, HIGH);
  digitalWrite(rellay2, HIGH);
  digitalWrite(rellay3, HIGH);
  digitalWrite(rellay4, HIGH);
  digitalWrite(rellay5, HIGH);
  digitalWrite(rellay6, HIGH);
}

 
void loop() {
  while (mySerial.available()) {
    //save received data from mySerial(LCD Nextion)
    char c = (char)mySerial.read(); 
    response += c;
  }//Loop working when Arduino receiving data from mySerial(LCD) 
  
  if (response.indexOf("st") >= 0) { 
    //if received message "st"

    //print response string data to Serial Monitor
    Serial.println(response);

    //execute CHECK_ALL() function
    CHECK_ALL(); 

    //set var 'response' to 0
    response = ""; 
  }
}
